#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
wmOpBot is a bot that help operators of Wikimedia channels at freenode IRC network.

@autor:   danilo (IRC) [[User:Danilo.mac]] (wiki)
@licença: GNU General Public License 3.0 (GPL V3)
"""

from twisted.words.protocols import irc
from twisted.internet import reactor, protocol

import re, time, os, socket, sys
import tools, warns
from database import *
from user import User

def decode(txt):
    "Decode the text, that can be in utf-8 or latim1 and encode in utf-8"
    try:
        txt = txt.decode('utf-8')
    except:
        txt = txt.decode('latin1')
    return txt.encode('utf-8')

reBot = re.compile(r''' # bots hosts #
        wiki\w{5,7}(?:/[]A-Za-z0-9^~[{}\\|]+)?/bot/[]A-Za-z0-9^~[{}\\|]+ | # wm cloak bots
        services\. | # ChanServ and NickServ
        freenode/utility-bot.* | # Sigyn and eir
        AntiSpamMeta/\. # AntiSpamMeta
        ''', re.X)

def isBot(user):
    return bool(reBot.match(user.host)) or tools.isBot(user)

temp = {}

class BotUser(irc.IRCClient):
    "Each bot nick (wmopbot, wmopbot2, etc) is an instance of this class"
    realname = 'wmopbot'

    def __init__(self, nickname):
        self.nickname = self.preferedNick = nickname
        self.channels = set()
        self.joinChans = ['#wikimedia-opbot']
        self.whoisQueue = []
        self.finishedJoin = False

    def signedOn(self):
        "Called after successfully signing on to the server "
        with open('.password') as f:
            password = f.read()
        self.msg('NickServ', 'IDENTIFY wmopbot ' + password)
        self.sendLine('MODE %s +R' % self.nickname)

    def authenticated(self):
        "Called when I have been authenticated"
        bot[self.preferedNick] = self
        if self.nickname != self.preferedNick:
            reactor.stop()
        else:
            self.joinManager()

    def joinManager(self):
        """
        Join channels flagged as KEEP, and query a WHO and WHOIS.
        Join one channel at a time, wait the end of NAMES
        replay, call a WHO, call WHOIS for cloaked users with
        unknown account and then join the next channel.
        """
        if self.joinChans:
            channel = self.joinChans[0]
            if not channel in self.channels:
                self.join(channel)
        elif not self.finishedJoin:
            self.finishedJoin = True
            bot.finishedJoin()

    def irc_JOIN(self, prefix, params):
        "Called when an user join a channel, params = ['#channel']"
        nick = prefix.split('!')[0]
        channel = irclower(params[0])
        chanNicks[channel][nick] = 0
        if nick == self.nickname:
            self.channels.add(channel)
            callhook('joined ' + channel)
            return
        user = User(prefix)
        if not user.account and not 'unreg' in user and not user.nick in self.whoisQueue:
            self.whoisQueue.append(user.nick)
            if len(self.whoisQueue) == 1:
                self.sendLine('WHOIS ' + user.nick)
        if not isBot(user):
            warns.join(user, channel)
        tools.join(user, channel)

    def msg(self, channel, message, *args):
        "Send a message to channel or user"
        if not isinstance(message, basestring):
            return
        if isinstance(message, unicode):
            message = message.encode('utf-8')
        irc.IRCClient.msg(self, channel, message, *args)

    def privmsg(self, user, channel, msg):
        "Called when I receive a message from a channel or user"
        if channel == '#wikimedia-opbot' and self.nickname != 'wmopbot':
            return # if more than one bot in the channel
        channel = irclower(channel)
        msg = decode(msg)
        user = User(user)
        if channel == '#wikimedia-opbot' and msg in ('!reload tools', '!reload warns') \
                and user.host == 'wikipedia/danilomac':
            if msg.startswith('!reload'):
                try:
                    reload(eval(msg[8:]))
                except Exception as e:
                    self.msg(channel, 'Error: ' + repr(e))
                else:
                    self.msg(channel, '%s.py reloaded' % msg[8:])
            return
        # Stop or restart the bot
        if channel in ('#wikimedia-ops', '#wikimedia-ops-internal', '#wikimedia-opbot') and \
                msg in ('!stop', '!restart') and \
                'o' in chanList['#wikimedia-ops']['access'].get(user.account, {}).get('flags', ''):
            print '[%s] command %s triggered by %s' % (time.strftime('%Y-%m-%d %H:%M:%S'), msg, user.full)
            bot.stop(msg == '!restart')
            return
        if isBot(user):
            if channel == self.nickname:
                tools.botPM(user, msg)
            else:
                tools.botChanMsg(user, channel, msg)
            return
        if channel == self.nickname:
            def run_command():
                command = tools.Command(msg, user, 'pm')
                if command.isvalid:
                    command.run()
                else:
                    tools.privateMsg(user, msg)
            if '/' in user.host and not user.account:
                print 'cloak but no account user pm: %s; %s' % (msg, user.full)
                addhook('whois ' + user.nick, run_command)
                self.sendLine('WHOIS ' + user.nick)
            else:
                run_command()
            return
        if msg[0] == '!':
            cmd = msg[1:]
        elif msg.startswith(self.nickname + ': '):
            cmd = msg[len(self.nickname) + 2:]
        else:
            warns.msg(user, channel, msg)
            tools.chanMsg(user, channel, msg)
            return
        command = tools.Command(cmd, user, channel)
        if command.isvalid:
            command.run()
        else:
            warns.msg(user, channel, msg)
            tools.chanMsg(user, channel, msg)

    def noticed(self, user, channel, message):
        "Called when I receive a notice"
        channel = irclower(channel)
        if not '!' in user and not self.finishedJoin:
            return
        if user == 'NickServ!NickServ@services.' and message.startswith('You are now identified'):
            print '[%s] %s authenticated' % (time.strftime('%Y-%m-%d %H:%M:%S'), self.nickname)
            self.authenticated()
            return
        user = User(user)
        if channel[0] == '#' and not isBot(user):
            warns.notice(user, channel, message)
        elif channel == self.nickname:
            if user.full == 'ChanServ!ChanServ@services.':
                tools.csNotice(message)
            else:
                tools.pmNotice(user, message)

    def irc_QUIT(self, user, params):
        "Called when someone quit (from the IRC, not from a channel), params = ['reason']"
        nick = user.split('!')[0]
        chans = []
        for chan in chanNicks:
            if nick in chanNicks[chan]:
                del chanNicks[chan][nick]
                chans.append(chan)
        if nick in nickUser:
            user = nickUser[nick]
            user.quit(params[0])
            warns.quit(user, chans, params[0])
            tools.quit(user, chans, params[0])

    def irc_PART(self, user, params):
        "Called when an user left a channel (not left IRC)"
        user = User(user)
        nick = user.nick
        channel = irclower(params[0])
        reason = len(params) > 1 and params[1] or ''
        if channel == '#wikimedia-opbot' and self.nickname != 'wmopbot':
            return
        if nick in chanNicks[channel]:
            del chanNicks[channel][nick]
        # if nick is me, check if another bot user is in the channel before discard channel
        if nick != self.nickname or [b for b in bot.nicks if b in chanNicks[channel]]:
            warns.part(user, channel, reason)
            return
        del chanNicks[channel]
        callhook('part ' + channel)
        tools.part(user, channel, reason)

    def irc_NICK(self, user, params):
        "Called when an user change the nick"
        old = user.split('!')[0]
        new = params[0]
        for chan in chanNicks:
            if old in chanNicks[chan]:
                chanNicks[chan][new] = chanNicks[chan][old]
                del chanNicks[chan][old]
        if old in nickUser:
            nickUser[new] = nickUser[old]
            del nickUser[old]
            nickUser[new].renamed(new)
        if old == self.nickname:
            self.nickname = new
        else:
            warns.renamed(nickUser[new], old)

    def irc_RPL_NAMREPLY(self, prefix, params):
        "Receives the list of nicks in channel, params = [self.nickname, '=', '#channel', 'nick1 @nick2 +nick3 ...']"
        channel = irclower(params[2])
        if not channel in chanNicks:
            chanNicks[channel] = {}
        for nick in params[3].split():
            status = nick[0] == '@' and 2 or nick[0] == '+' and 1 or 0
            chanNicks[channel][nick.strip('@+')] = status

    def irc_RPL_ENDOFNAMES(self, prefix, params):
        "End of names replay, params = [self.nickname, '#channel', 'End of /NAMES list.']"
        self.sendLine('WHO ' + irclower(params[1]))

    def irc_RPL_WHOREPLY(self, prefix, params):
        """Receives WHO data, params = [self.nickname, '#channel', 'ident', 'host', 'server', 'nick',
           '{H|G}[@|+]', '0 realname']"""
        channel = irclower(params[1])
        nick = params[5]
        user = User('%s!%s@%s' % (nick, params[2], params[3]), realname=params[7][2:])
        chanNicks[channel][nick] = ('@' in params[6] and 2 or 0) + ('+' in params[6] and 1 or 0)

    def irc_RPL_ENDOFWHO(self, prefix, params):
        "End of WHO replay, params = [self.nickname, '#channel', 'End of /WHO list.']"
        channel = irclower(params[1])
        if channel in self.joinChans:
            self.joinChans.remove(channel)
        unknownAcc = [user.nick for user in (nickUser[nick] for nick in chanNicks[channel] if nick in nickUser)
                if not user.account and not 'unreg' in user]
        if unknownAcc:
            self.whoisQueue.extend(unknownAcc)
            self.sendLine('WHOIS ' + unknownAcc[0])
        else:
            self.joinManager()
        callhook('who ' + channel)

    def irc_RPL_WHOISUSER(self, prefix, params):
        "Receives WHOIS user data, params = [self.nickname, 'nick', 'ident', 'host', '*', 'realname']"
        user = User('%s!%s@%s' % (params[1], params[2], params[3]), realname=params[5])

    def irc_RPL_WHOISCHANNELS(self, prefix, params):
        "Receives WHOIS channel data, params = [self.nickname, 'nick', '#chanel1 #channel2 @#chanel_nick_is_op ...']"

    def irc_330(self, prefix, params):
        "Receives WHOIS user account data, params = [self.nickname, 'nick', 'account', 'is logged in as']"
        nick, account = params[1:3]
        nickUser[nick]['account'] = account

    def irc_RPL_ENDOFWHOIS(self, prefix, params):
        "End of WHOIS list, params = [self.nickname, 'nick', 'End of /WHOIS list.']"
        nick = params[1]
        if nick in self.whoisQueue:
            self.whoisQueue.remove(nick)
            if self.whoisQueue:
                self.sendLine('WHOIS ' + self.whoisQueue[0])
            else:
                self.joinManager()
        user = nickUser.get(nick)
        if user and not 'account' in user:
            user['unreg'] = '%d' % time.time()
        elif user and 'account' in user and 'unreg' in user:
            del user['unreg']
        callhook('whois ' + nick)

    def irc_RPL_BANLIST(self, prefix, params):
        "Receives ban list data,  params = [self.nickname, '#channel', 'mask', 'op!who@banned', 'unix timestamp']"
        channel = tools.irclower(params[1])
        banlist = chanList[channel]['ban']
        if params[2] not in banlist:
            banlist[params[2]] = {'user': '!' in params[3] and params[3] or '?', 'time': params[4]}
        temp.setdefault('ban' + channel, set()).add(params[2])

    def irc_RPL_ENDOFBANLIST(self, prefix, params):
        "End of ban list"
        channel = tools.irclower(params[1])
        chanList[channel]['config'].setdefault('update', {})['ban'] = str(int(time.time()))
        for target in chanList[channel]['ban'].viewkeys() - temp.pop('ban' + channel, set()):
            del chanList[channel]['ban'][target]
            if '+b ' + target in chanList[channel]['mode']:
                del chanList[channel]['mode']['+b ' + target]
        callhook('ban list ' + channel)

    def irc_728(self, prefix, params):
        "Receives quiet list data,  params = [self.nickname, '#channel', 'q', 'mask', 'op!who@quieted', 'unix timestamp']"
        channel = irclower(params[1])
        quitelist = chanList[channel]['quiet']
        if params[3] not in quitelist:
            quitelist[params[3]] = {'user': '!' in params[4] and params[4] or '?', 'time': params[5]}
        temp.setdefault('quiet' + channel, set()).add(params[3])

    def irc_729(self, prefix, params):
        "End of quiet list"
        channel = irclower(params[1])
        chanList[channel]['config'].setdefault('update', {})['quiet'] = str(int(time.time()))
        for target in chanList[channel]['quiet'].viewkeys() - temp.pop('quiet' + channel, set()):
            del chanList[channel]['quiet'][target]
            if '+q ' + target in chanList[channel]['mode']:
                del chanList[channel]['mode']['+q ' + target]
        callhook('quiet list ' + channel)

    def irc_RPL_EXCEPTLIST(self, prefix, params):
        "Receives except list data,  params = [self.nickname, '#channel', 'mask', 'op!who@excepted', 'unix timestamp']"
        channel = irclower(params[1])
        exceptlist = chanList[channel]['except']
        if params[2] not in exceptlist:
            exceptlist[params[2]] = {'user': '!' in params[3] and params[3] or '?', 'time': params[4]}
        temp.setdefault('except' + channel, set()).add(params[2])

    def irc_RPL_ENDOFEXCEPTLIST(self, prefix, params):
        "End of except list"
        channel = irclower(params[1])
        chanList[channel]['config'].setdefault('update', {})['except'] = str(int(time.time()))
        callhook('except list ' + channel)
        for target in chanList[channel]['except'].viewkeys() - temp.pop('except' + channel, set()):
            del chanList[channel]['except'][target]

    def irc_RPL_CHANNELMODEIS(self, prefix, params):
        "Receives channel modes, params = [self.nickname, '#channel', 'modes']"
        channel = irclower(params[1])
        chanList[channel]['config'].setdefault('cs/irc', {})['mode'] = params[2]
        chanList[channel]['config'].setdefault('update', {})['mode'] = str(int(time.time()))
        for mode in chanList[channel]['mode'].keys():
            if not mode[1] in 'qb' and not mode[1] in params[2]:
                del chanList[channel]['mode'][mode]
        callhook('mode ' + channel)

    def irc_329(self, prefix, params):
        "Receives channel create time, params = [self.nickname, '#channel', 'timestamp']"
        channel = irclower(params[1])
        chanList[channel]['config'].setdefault('cs/irc', {})['created'] = params[2]

    def irc_RPL_TOPIC(self, prefix, params):
        "Receives channel topic"
        channel = irclower(params[1])
        topics[channel] = params[2]

    def irc_333(self, prefix, params):
        "Receives user who set the topic and timestamp"
        channel = irclower(params[1])
        topics['who' + channel] = ' '.join(params[2:])

    def irc_TOPIC(self, prefix, params):
        "Called when an user change a channel topic"
        channel = irclower(params[0])
        topics[channel] = params[1]
        topics['who' + channel] = '%s %d' % (prefix, time.time())
        user = nickUser[prefix.split('!')[0]]
        warns.topic(user, channel, params[1])

    def irc_ERR_NOSUCHCHANNEL(self, prefix, params):
        """Called when try to get data about a channel that does not exist,
           params = [self.nickname, '#channel', 'No such channel']"""
        channel = irclower(params[1])
        if channel in chanList:
            t = str(int(time.time()))
            chanList[channel]['config'].setdefault('update', {}).update({'ban': t, 'quiet': t, 'mode': t})
            chanList[channel]['config']['update']['error'] = 'no such channel'
        callhook('no such channel ' + channel)

    def irc_ERR_NOSUCHNICK(self, prefix, params):
        """Called when I send message to a nick that is not online,
           params = [self.nickname, 'nick', 'No such nick/channel']"""
        nick = irclower(params[1])
        callhook('no such nick ' + nick)

    def irc_ERR_INVITEONLYCHAN(self, prefix, params):
        """Called when attempt to join an invite only channel,
           params = [self.nickname, '#channel', 'Cannot join channel (+i) - you must be invited']"""
        channel = irclower(params[1])
        if channel in self.joinChans:
            self.joinChans.remove(channel)
            bot.msg('#wikimedia-opbot', 'Can\'t join %s, invite only channel' % channel)
            self.joinManager()
        callhook('invite only ' + channel)
    
    def irc_ERR_CHANOPRIVSNEEDED(self, prefix, params):
        """Called when try to execute a command restricted to op and I am not
           params = [self.nickname, '#channel', 'You're not a channel operator']"""
        channel = irclower(params[1])
        callhook('not op ' + channel)
        print '[%s] Tried to execute an command restricted to op in %s but I am not' % \
                (time.strftime('%Y-%m-%d %H:%M:%S'), channel)

    def irc_MODE(self, user, params):
        """Receives channel mode change (+b, +q, +I, +e, +r, +m, +o, +v, -b, -q, ...)
           params = ['#channel', 'mode', 'arguments (ban mask, nick, none, ...)']"""
        if not '!' in user:
            return
        if params[0] == '#wikimedia-opbot' and self.nickname != 'wmopbot':
            return
        user = User(user)
        channel = irclower(params[0])
        sign, mode = params[1][0], params[1][1:]
        args = params[2:]
        for m in mode:
            if m in '+-':
                sign = m
                continue
            if m in 'bqeI':
                target = args.pop(0)
                tools.modeUser(user, channel, sign + m, target)
                if sign + m == '+b':
                    warns.banned(user, channel, target)
            elif m in 'ov':
                nick = args.pop(0)
                if nick in chanNicks[channel]:
                    chanNicks[channel][nick] += {'+o': 2, '-o': -2, '+v': +1, '-v': -1}[sign + m]
                if nick in bot.nicks:
                    callhook({'+o': 'op ', '-o': 'deop ', '+v': 'voice ', '-v': 'devoice '}[sign + m] + channel)
            elif channel[0][0] == '#' and sign == '+' and m in 'fjkl':
                tools.chanMode(user, channel, sign + m, args.pop(0))
            elif channel[0][0] == '#':
                tools.chanMode(user, channel, sign + m)

    def irc_KICK(self, user, params):
        "Called when an user is kicked from a channel; params = ['#channel', 'nick', 'reason']"
        if params[0] == '#wikimedia-opbot' and self.nickname != 'wmopbot':
            return
        user = User(user)
        channel = irclower(params[0])
        target = nickUser[params[1]]
        reason = len(params) == 3 and params[2] or ''
        nick = target.nick
        if nick in chanNicks[channel]:
            del chanNicks[channel][nick]
        # if nick is me, check if another bot user is in the channel before discard channel
        if nick == self.nickname and not [b for b in bot.nicks if b in chanNicks[channel]]:
            del chanNicks[channel]
            callhook('part ' + channel)
        warns.kicked(user, channel, target, reason)
        tools.kicked(user, channel, target, reason)

    def irc_unknown(self, prefix, command, params):
        "Called when receives an unknown message from server"
        if command in ('PONG', '328', '671', 'RPL_WHOISSERVER', 'RPL_AWAY', 'RPL_WHOISIDLE'):
            return
        if prefix.endswith('freenode.net'):
            print command, params
        else:
            print prefix, command, params

    def handleCommand(self, command, prefix, params):
        "Wraper the handleCommand function to register the command process time"
        start = time.time()
        irc.IRCClient.handleCommand(self, command, prefix, params)
        bot.cmdtime(time.time() - start, command, prefix, params)

class BotFactory(protocol.ClientFactory):
    "Factory to connect and reconnect if needed the bot users instances"

    def __init__(self, nickname):
        self.nickname = nickname

    def buildProtocol(self, addr):
        "Called when a new connection is established, return a new bot user instance"
        p = BotUser(self.nickname)
        print '[%s] BuildProtocol Bot(%s)' % (time.strftime('%Y-%m-%d %H:%M:%S'), self.nickname)
        return p

    def clientConnectionLost(self, connector, reason):
        "Called when the connections is lost"
        print '[%s] freenode connection lost for' % time.strftime('%Y-%m-%d %H:%M:%S'), self.nickname, reason
        if not bot.stopped:
            reactor.callLater(10, bot.stop, True)

    def clientConnectionFailed(self, connector, reason):
        "Called when failed to connect"
        print '[%s] Connection failed: %s' % (time.strftime('%Y-%m-%d %H:%M:%S'), reason)
        bot.connectionFailed(self.nickname, connector)
        reactor.callLater(600, connector.connect)

class BotManager:
    "Class to manage bot accounts to work as one bot"

    def __init__(self):
        "Load all data and configure the bots connections"
        self.nicks = {}
        self.factories = {}
        self.connectors = {}
        tools.bot = warns.bot = self
        self.stopped = False
        self.nextChecks = reactor.callLater(600, self.periodic)
        self.queryError = None
        self.queryQueue  = []
        self.queryRunning = False
        self.keepChans = {}
        nicks = ['wmopbot' + n for n in ('', '2', '3', '4', '5')]
        keepChans = [chan for chan in chanList if 'KEEP' in chanList[chan]['config']]
        for nick in nicks:
            # Limit 120 channels per connection
            self.keepChans[nick] = {'#wikimedia-opbot', '#wikimedia-opbot-test' + nick[7:]} | set(keepChans[:118])
            del keepChans[:118]
            self.createBotUser(nick)
            if not keepChans:
                break
        self.started = False
        self.restart = False
        self.temp = {}
        self.lastTemp = []
        self.nextCheck = None
        self.warns = {}
        self.lostUsers = []
        self.allowedJoin = True
        self.cmdTime = [0, 0, 0, 0]
        self.cmdLong = {}
        print '[%s] Started bot' % time.strftime('%Y-%m-%d %H:%M:%S')

    def createBotUser(self, nickname):
        if len(self.factories) == 5:
            print '[%s] Tried to create more than 5 bot connections (%s)' % (time.strptime('%Y-%m-%d %H:%M:%S'), nickname)
            return
        self.factories[nickname] = BotFactory(nickname)
        reactor.connectTCP("irc.freenode.net", 6667, self.factories[nickname])

    def __setitem__(self, nickname, botInstance):
        "Called when the bot account has been authenticated"
        self.nicks[nickname] = botInstance

    def __getitem__(self, nickname):
        "Returns a bot account instance if it is connected, or None"
        return self.nicks.get(nickname)

    def __contains__(self, nickname):
        return nickname in self.nicks

    def __delitem__(self, nickname):
        "Called when a bot nick connection is lost"
        if nickname in self.nicks:
            del self.nicks[nickname]

    @property
    def main(self):
        "Returns the main bot connection, usualy the wmopbot"
        return self.nicks and self.nicks[sorted(self.nicks)[0]] or None

    def connectionFailed(self, nickname, connector):
        "Called when a bot nick failed to connect"
        self.connectors[nickname] = connector
        self.msg('#wikimedia-opbot', '%s \x0304connection failed' % nickname)

    def chooseBot(self, channel):
        "Choose the bot (or bots) who will send a message or notice"
        chans, privs = [], []
        for c in channel.split(','):
            if c.startswith(('#', '+#', '@#')):
                chans.append(c)
            else:
                privs.append(c)
        botChans = {}
        if privs:
            botChans[sorted(bot.nicks)[0]] = privs
        if chans:
            for nick in sorted(self.nicks):
                for chan in list(chans):
                    if nick in chanNicks[chan.strip('+@')]:
                        chans.remove(chan)
                        botChans.setdefault(nick, []).append(chan)
        r = [(b, ','.join(c)) for b, c in botChans.items()]
        return r

    def msg(self, channel, message):
        "Send a message using the bot that is in the channel"
        for nick, chans in self.chooseBot(channel):
            self[nick].msg(chans, message)

    def notice(self, channel, message):
        "Send a notice using the bot that is in the channel"
        for nick, chans in self.chooseBot(channel):
            self[nick].sendLine('NOTICE %s :%s' % (chans, message))
        
    def sendLine(self, line, channel=None):
        "Send line to server using the bot that is in channel"
        chan = re.search(r'[+-]?#[]A-Za-z0-9[{}\\|~^-_#]+', line)
        if chan:
            channel = chan.group(0)
        if channel:
            nicks = [n for n in chanNicks.get(channel, ()) if n in self.nicks]
            if len(nicks) >= 1:
                self[sorted(nicks)[0]].sendLine(line)
                return
        if self.nicks:
            self.main.sendLine(line)

    def join(self, channel):
        "Select which bot will join the channel"
        keepNicks = [nick for nick in self.keepChans if channel in self.keepChans[nick]]
        if keepNicks:
            for nick in keepNicks:
                if not nick in chanNicks.get(channel, ()):
                    self[nick].sendLine('JOIN ' + channel)
            return
        for nick in sorted(self.nicks):
            if len(self.keepChans[nick]) < 120:
                self.keepChans[nick].add(channel)
                self[nick].joinChans.append(channel)
                self[nick].joinManager()
                return
        if len(self.factories) < 5:
            nick = 'wmopbot' + str(len(self.factories) + 1)
            self.keepChans[nick] = {'#wikimedia-opbot', '#wikimedia-opbot-test' + nick[7:], channel}
            self.createBotUser(nick)
        else:
            self.msg('#wikimedia-opbot', 'I can\'t join more channels, limit achieved')

    def part(self, channel):
        "Part the channel"
        keepNicks = [nick for nick in self.keepChans if chan in self.keepChan[nick]]
        if keppNicks:
            for nick in keepNicks:
                self.keepChans[nick].discard(channel)
                if channel in self[nick].joinChans:
                    self[nick].joinChans.remove(channel)
        if channel in chanNicks:
            for nick in [nick for nick in self.nicks if nick in chanNicks[channel]]:
                self[nick].sendLine('PART ' + channel)

    def stop(self, restart=False):
        "Save all unsaved data, make bots quit IRC and stop reactor"
        print 'bot.stop(%r)' % restart
        self.stopped = True
        self.restart = restart
        reactor.callLater(3, reactor.stop)
        dbSync()
        for nick in self.nicks:
            self[nick].sendLine('QUIT ' + (restart and 'Restarting' or 'Stopped'))

    def periodic(self):
        "Make and trigger periodic checks"
        # Every 10 minutes, reset the timer if the function is called manualy
        if self.nextCheck and self.nextCheck.active():
            self.nextChecks.reset(600)
        else:
            self.nextChecks = reactor.callLater(600, self.periodic)
        lost = []
        # Users in nickUser that is in no channel are deleted only after two periodic checks
        for nick in nickUser.viewkeys() - {n for c in chanNicks for n in chanNicks[c]}:
            if nick in self.lostUsers:
                del nickUser[nick]
            else:
                lost.append(nick)
        self.lostUsers = lost
        for nick in {n for c in chanNicks for n in chanNicks[c]}:
            if not nick in nickUser:
                continue
            nickUser[nick]['score'] = nickUser[nick].get('score', 0) + 1
        start = time.time()
        dbSync()
        syncTime = 'dbSync: %f sec' % (time.time() - start)
        start = time.time()
        tools.periodic()
        print time.strftime('[%Y-%m-%d %H:%M:%S]') + ' periodic: ' + syncTime + ', tools.periodic: %f sec' \
                % (time.time() - start)
        for i in self.lastTemp:
            if i in self.temp:
                del self.temp[i]
        self.lastTemp = self.temp.keys()
        sys.stdout.flush()

    def finishedJoin(self):
        "Called when a bot user finished join your channels list"
        if any(self[nick].joinChans for nick in self.nicks):
            return
        if not self.started and tools.allowJoin and len({c for n in self.nicks for c in self[n].channels}) == 1:
            self.joinAll()
        elif not self.started:
            sys.stdout.flush()
            self.started = True
            nchans = len(reduce(lambda a, b: a | b, (self[nick].channels for nick in self.nicks)))
            nusers = len({n for c in chanNicks for n in chanNicks[c]})
            self.msg('#wikimedia-opbot', 'finished join %d channels, I see %d users' % (nchans, nusers))
            tools.initialized()

    def joinAll(self):
        "Join all channels configured as keep channels"
        for nick in self.nicks:
            self[nick].joinChans = list(self.keepChans[nick] - self[nick].channels)
            self[nick].finishedJoin = False
            self[nick].joinManager()

    def query(self, q=None):
        "Add a query to query queue and/or call the next query"
        if q:
            self.queryQueue.append(q)
        if self.queryRunning and self.queryRunning[0] + 30 > time.time() or not self.queryQueue:
            return
        nextq = self.queryQueue.pop(0)
        def done(err=None):
            self.queryRunning = False
            if self.queryError and self.queryError.active():
                self.queryError.cancel()
            self.query()
        for item in queryCmd:
            query, cmd = item[:2]
            if nextq.startswith(query):
                chan = nextq[len(query):].strip()
                if not self.main:
                    sys.stderr.write('self.main error, self.nicks: %r' % self.nicks)
                self['wmopbot'].sendLine(cmd % chan)
                errhooks = ['%s' in h and h % chan or h for h in item[2:]]
                addhook(nextq, done, *errhooks)
                self.queryRunning = (time.time(), nextq)
                self.queryError = reactor.callLater(15, sys.stderr.write, 'Error: query timeout: %s\n' % nextq)
                break
        else:
            self.query()

    def remove(self, channel):
        "remove expired ban, quiet or mode"
        remove = []
        modes = ''
        args = []
        commands = []
        now = int(time.time())
        nicks = {nick: status for nick, status in chanNicks[channel].items() if nick in self.nicks}
        if [s for s in nicks.values() if s >= 2]:
            botuser = self[[n for n in nicks if nicks[n] >= 2][0]]
        elif nicks and 'o' in chanList[channel]['access'].get('wmopbot', {}).get('flags', ''):
            botuser = self[sorted(nicks)[0]]
        else:
            print '[%s] Could not remove ban/quite/mode from %s, %s' % (time.strftime('%Y-%m-%d %H:%M:%S'), channel,
                    nicks and 'I am not op' or 'I am not in channel')
            return
        for mode in ('ban', 'quiet'):
            for target in chanList[channel][mode]:
                expiry = chanList[channel][mode][target].get('expiry')
                if expiry and expiry != 'p' and int(expiry) < now:
                    remove.append((mode[0], target))
        if remove and nicks[botuser.nickname] != 2:
           remove.append(('o', 'wmopbot'))
        while remove:
            m, arg = remove.pop(0)
            modes += m
            if arg:
                args.append(arg)
            if len(modes) == 4:
                commands.append('MODE %s -%s %s' % (channel, modes, ' '.join(args)))
                modes, args = '', []
        if modes:
            commands.append('MODE %s -%s %s' % (channel, modes, ' '.join(args)))
        if not commands or not nicks:
            return
        def oped():
            for cmd in commands:
                botuser.sendLine(cmd)
        if nicks[botuser.nickname] == 2:
            oped()
        else:
            addhook('op ' + channel, oped)
            botuser.sendLine('CS OP ' + channel)


    def cmdtime(self, t, command, prefix, params):
        "Log commands process time"
        if t > 0.2:
            print 'Long command %fs %s %s %r' % (t, command, prefix, params)
            self.cmdTime[0:2] = [self.cmdTime[0] + 1, self.cmdTime[1] + t]
        else:
            self.cmdTime[2:4] = [self.cmdTime[2] + 1, self.cmdTime[3] + t]

queryCmd = (('ban list', 'MODE %s +b', 'no such channel %s'),
        ('quiet list', 'MODE %s +q', 'no such channel %s'),
        ('access', 'CS ACCESS %s LIST', 'cs not authorized', 'not registered %s'),
        ('csinfo', 'CS INFO %s', 'cs not authorized', 'not registered %s'),
        ('mode', 'MODE %s', 'no such channel %s'),
        ('eir', 'PRIVMSG eir :btinfo %s', 'eir no results', 'no such nick eir'))

if __name__ == '__main__':
    bot = BotManager()
    reactor.run()
    print 'Reactor stopped'
    if bot.restart:
        sys.exit(1) # Exit with code 1 to force Grid Engine restart the bot
    else:
        sys.exit(0)
