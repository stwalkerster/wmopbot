# -*- coding: utf-8 -*-

from database import *
import tools

changingHost = {}

def User(nickIdentHost, **args):
    "return an existing UserClass instance or create a new one"
    if isinstance(nickIdentHost, UserClass):
        return nickIdentHost
    if not isinstance(nickIdentHost, basestring) or not '!' in nickIdentHost or not '@' in nickIdentHost:
        raise ValueError('User must be initialized with "nick!ident@host", got %r' % nickIdentHost)
    nick = nickIdentHost.split('!')[0]
    if nick in nickUser:
        user = nickUser[nick]
        if args:
            user.update(args)
        return user
    if changingHost:
        for nickIdent, user in changingHost.items():
            if int(user['quit']) < time.time() - 5:
                del changingHost[nickIdent]
                continue
            if nickIdentHost.split('@')[0] == nickIdent:
                del changingHost[nickIdent]
                user.pop('quit', None)
                user.pop('quit reason')
                user['host'] = nickIdentHost.split('@')[1]
                return user
    dbUser = db.query('SELECT * FROM user WHERE us_user = ?', (nickIdentHost,))
    if dbUser:
        return UserClass.fromDB(nickIdentHost, dbUser[0])
    return UserClass(nickIdentHost, args)

reIP = re.compile(r'(\d{1,3}[.-]\d{1,3}[.-]\d{1,3}[.-]\d{1,3}|[0-9a-f]{1,4}(?:::?[0-9a-f]{1,4}){1,8}(?:::)?)', re.I)

class UserClass(dict):

    def __init__(self, nickIdentHost, args=None):
        self['nick'] = nickIdentHost.split('!')[0]
        self['ident'], self['host'] = nickIdentHost.split('!')[1].split('@')
        ip = reIP.search(self.host)
        if ip:
            self['ip'] = ip.group(1).replace('-', '.')
        elif '/' in self.host and not self.host.startswith('gateway/'):
            self['cloak'] = self.host.split('/')[0]
        if args:
            self.update(args)
        nickUser[self.nick] = self
        self.issaved = False

    def __setitem__(self, key, value):
        self.issaved = False
        super(UserClass, self).__setitem__(key, value)

    def __delitem__(self, key):
        self.issaved = False
        super(UserClass, self).__delitem__(key)

    def __getattr__(self, name):
        return self.get(name)

    @property
    def full(self):
        return '%(nick)s!%(ident)s@%(host)s' % self

    @property
    def score(self):
        return self.get('score', 0)

    @classmethod
    def fromDB(cls, nickIdentHost, dbArgs):
        oldNickIdentHost, account, ip, realname, seen, score, extra = dbArgs
        user = cls(nickIdentHost)
        score = score and int(score)
        extra = [(k, v.replace('\;', ';')) for k, v in (i.split(':', 1) for i in
                re.split(r'^;|(?<=[^\\]);', extra or '') if ':' in i)]
        for k, v in [('account', account), ('ip', ip), ('realname', realname), ('seen', seen), ('score', score)] + extra:
            if v:
                user[k] = v
        return user

    def toDB(self):
        extra = {k: v for k, v in self.iteritems() if k not in
                ('nick', 'ident', 'host', 'account', 'ip', 'realname', 'seen', 'score')}
        return (self.full, self.get('account'), self.get('ip'), self.get('realname'), self.get('score', 0),
                extra and dumpArgs(extra) or None)

    def saved(self):
        "Called when the user data is saved to database"
        self.issaved = True

    def quit(self, reason):
        "User quit from IRC"
        if reason == 'Changing host':
            changingHost['%(nick)s!%(ident)s'] = self
        self['quit'] = '%d' % time.time()
        if reason:
            self['quit reason'] = reason
        nickUser.pop(self.nick, None)
        extra = {k: v for k, v in self.iteritems() if k not in
                ('nick', 'ident', 'host', 'account', 'ip', 'realname', 'seen', 'score')}
        db.execute('REPLACE INTO user VALUES (?, ?, ?, ?, NOW(), ?, ?)', (self.full, self.account, self.get('ip'),
                self.get('realname'), self.get('score', 0), extra and dumpArgs(extra) or None))

    def renamed(self, new):
        "User changed nick"
        extra = {k: v for k, v in self.iteritems() if k not in
                ('nick', 'ident', 'host', 'account', 'ip', 'realname', 'seen', 'score')}
        db.execute('REPLACE INTO user VALUES (?, ?, ?, ?, NOW(), ?, ?)', (self.full, self.account, self.get('ip'),
                self.get('realname'), self.get('score', 0), extra and dumpArgs(extra) or None))
        self['nick'] = new
        self['score'] = 0
        dbScore = db.query('SELECT us_score FROM user WHERE us_user = ?', (self.full,))
        if dbScore and dbScore[0][0]:
            self['score'] = int(dbScore[0][0])
