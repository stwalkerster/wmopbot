var ns = 'http://www.w3.org/2000/svg';
function line7days(svg, data){
  var xmax = Math.floor(Date.now() / 3600000);
  var xmin = xmax - 168;
  var grid = document.createElementNS(ns, 'g');
  var xdelta = (xmax - Math.floor(Date.now() / 86400000) * 24) * 4;
  grid.className = 'grid';
  grid.style.stroke = '#eee';
  grid.style.strokeWidth = 1;
  for (var i = 1; i <= 7; i++){ // x grid ||||
    var line = document.createElementNS(ns, 'line');
    line.setAttributeNS(null, 'x1', 50 - xdelta + 96 * i);
    line.setAttributeNS(null, 'y1', 50);
    line.setAttributeNS(null, 'x2', 50 - xdelta + 96 * i);
    line.setAttributeNS(null, 'y2', 250);
    grid.appendChild(line);
  }
  for (var i = 1; i <= 5; i++){ // y grid ====
    var line = document.createElementNS(ns, 'line');
    line.setAttributeNS(null, 'x1', 50);
    line.setAttributeNS(null, 'y1', 50 * i);
    line.setAttributeNS(null, 'x2', 722);
    line.setAttributeNS(null, 'y2', 50 * i);
    grid.appendChild(line);
  }
  svg.style.width = '772px';
  svg.appendChild(grid);
  var datamin = Math.min.apply(null, Object.keys(data));
  var yscale = 0.5;
  for (i in data) {
    if (yscale * 200 < data[i]) yscale = yscale * 2;
  }
  var d = 'M' + (50 + (datamin - xmin) * 4) + ',250';
  var points = '';
  for (var i = datamin; i <= xmax; i++){
    var x = 50 + (i - xmin) * 4;
    var y = 250 - (i in data ? data[i] : 0) / yscale;
    d += ' L' + x + ',' + y;
    points += ' ' + x + ',' + y;
  }
  d += 'L' + (50 + (i - xmin) * 4) + ',250 Z';
  var path = document.createElementNS(ns, 'path');
  path.setAttributeNS(null, 'd', d);
  path.style.fill = '#e80';
  path.style.fillOpacity = 0.2;
  svg.appendChild(path);
  var polyline = document.createElementNS(ns, 'polyline');
  polyline.setAttributeNS(null, 'points', points);
  polyline.style.fill = 'none';
  polyline.style.stroke = '#e80';
  polyline.style.strokeWidth = 2;
  svg.appendChild(polyline);
  var text = document.createElementNS(ns, 'g');
  var tick = document.createElementNS(ns, 'text');
  tick.setAttributeNS(null, 'x', 30);
  tick.setAttributeNS(null, 'y', 157);
  tick.fill = 'black';
  tick.innerHTML = '100';
  text.appendChild(tick);
  var text = document.createElementNS(ns, 'text');
  text.setAttributeNS(null, 'x', 386);
  text.setAttributeNS(null, 'y', 30);
  text.setAttributeNS(null, 'text-anchor', 'middle');
  text.innerHTML = 'Human users messages per hour in the last 7 days';
  for (var i = 0; i < 5; i++){
    var tick = document.createElementNS(ns, 'tspan');
    tick.setAttributeNS(null, 'x', 45);
    tick.setAttributeNS(null, 'y', 255 - 50 * i);
    tick.setAttributeNS(null, 'font-size', 10);
    tick.setAttributeNS(null, 'text-anchor', 'end');
    tick.innerHTML = 50 * i * yscale;
    text.appendChild(tick);
  }
  svg.appendChild(text);
}
