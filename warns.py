# -*- coding: utf-8 -*-

from database import *
import tools

reIdentIP = re.compile(r'!([0-9a-fA-F]{8})@.*/.*')
reASCII = re.compile(r'\||  ')

def loadbl():
    "Load the blacklist"
    global blacklist
    terms = [t[0] == '/' and t[-1] == '/' and t.strip('/') or '\\b' + re.escape(t)
            for t in chanList['global'].get('black', ())]
    try:
        blacklist = re.compile('|'.join(terms))
    except:
        blacklist = None
        bot.msg('#wikimedia-opbot', 'Error while compiling blacklist')

if not 'recentActions' in globals():
    recentActions = defaultdict(lambda: deque(maxlen=30))
    chanWarns = defaultdict(list)
    warnDelay = {}
    loadbl()
    chanDisabled = {}
    tracked = {}
    recentKRB = deque(maxlen=10)
    warned = deque(maxlen=20)
    warnedKRB = deque(maxlen=10)

def mkping(channel, flag=None, inchan=['#wikimedia-opbot', '#wikimedia-ops'], noflag=None):
    "Return a set of channel ops nicks"
    if not channel in chanList or channel == '#wikimedia-overflow':
        return set()
    if isinstance(inchan, basestring):
        inchan = [inchan]
    accounts = {op for op in chanList[channel]['access'] if 'o' in chanList[channel]['access'][op]['flags'].lower()} - \
            {'Sigyn', 'eir'}
    if any(1 for entry in accounts if entry.startswith('$chanacs:')):
        for entry in [entry for entry in accounts if entry.startswith('$chanacs:')]:
            chan = entry[9:]
            if chan in chanList:
                accounts |= {a for a in chanList[chan]['access'] if chanList[chan]['access'][a]['flags'] != 'b'}
    if flag and 'ping' in chanList['global']:
        flag = set(flag)
        accounts &= {a for a, args in chanList['global']['ping'].iteritems() if flag & set(args.get('flags', ''))}
    if noflag and 'ping' in chanList['global']:
        noflag = set(noflag)
        accounts -= {a for a, args in chanList['global']['ping'].iteritems() if noflag & set(args.get('flags', ''))}
    nicks = {nick for chan in inchan for nick in chanNicks.get(chan, ())}
    return {nick for nick in nicks if nickUser.get(nick, {}).get('account') in accounts}

def warn(target, action, channel, ping=False, warnChan=None, delay=3):
    ping = ping and mkping(channel, ping) or set()
    for wnd in warned:
        if wnd[0:3] == (target, action, channel) and wnd[-1] + 30 > time.time():
            print 'Warn Throttled: %s, %s, %s [%s]' % (target, action, channel, time.strftime('%H:%M:%S'))
            return
    warned.append((target, action, channel, time.time()))
    if channel in ('#wikimedia-overflow', '#wikimedia-ops'):
        warnChan = None
    channel = type(channel) == list and channel or channel and [channel] or []
    for chan in ['#wikimedia-opbot'] + (warnChan and (isinstance(warnChan, basestring) and [warnChan] or warnChan) or []):
        if chan in chanDisabled and ('all' in chanDisabled[chan] or not set(channel) - chanDisabled[chan]):
            print 'Warn disabled in %s: %s %s %s' % (chan, target, action, mklist(channel))
            continue
        chanWarns[chan].append((target, action, channel, ping, time.time()))
    if delay:
        if not target in warnDelay:
            warnDelay[target] = reactor.callLater(delay, sendWarns, target)
    else:
        sendWarns(target)
        

def sendWarns(target):
    if target in warnDelay:
        if warnDelay[target].active():
            warnDelay[target].cancel()
        del warnDelay[target]
    now = time.time()
    # warn -> ('target', 'action', 'channel', set(ping), float(time))
    # select all warns of the same target
    targetWarns = [(chan, w) for chan in chanWarns for w in chanWarns[chan] if w[0] == target and w[4] + 10 > now]
    if not targetWarns:
        return
    uniqueWarns = []
    # merge warns to warn in the same channel
    for chan in {c for c, w in targetWarns}:
        cwarns = [w for c, w in targetWarns if c == chan]
        # if same target has warns with different actions in different channels, split the warn per action
        if len({w[1] for w in cwarns}) > 1 and len({repr(w[2]) for w in cwarns}) > 1:
            for action in {w[1] for w in cwarns}:
                uniqueWarns.append((chan, [w for w in cwarns if w[1] == action]))
        else:
            uniqueWarns.append((chan, cwarns))
    chansUnique = []
    for chan, warns in uniqueWarns:
        # if it is only one warn, add other warns with different target but same action and channel
        if len(warns) == 1:
            warns += [w for w in chanWarns[chan]
                    if w[0] != target and w[1] == warns[0][1] and w[2] == warns[0][2] and w[4] + 10 > now]
        for w in warns:
            chanWarns[chan].remove(w)
        temp = []
        nicks = mklist([w[0] for w in warns if not (w[0] in temp or temp.append(w[0]))])
        actions = mklist([w[1] for w in warns if not (w[1] in temp or temp.append(w[1]))])
        actions = re.sub(r' (?:in|from)(?=, | and)', '', actions)
        channels = mklist({c for w in warns for c in w[2]})
        ping = set() if '\x0303' in actions else {n for w in warns for n in w[3]}
        # merge identic warns to send to diferent channels in one command (/PRIVMSG #chan1,#chan2 :message)
        # in order to avoid disconnection per Excess Flood
        for i, items in enumerate(chansUnique):
            if [nicks, actions, channels] == items[1:4]:
                chansUnique[i][0] += ',' + chan
                chansUnique[i][4] |= ping
                break
        else:
            chansUnique.append([chan, nicks, actions, channels, ping])
    for chan, nicks, actions, channels, ping in chansUnique:
        ping = ' '.join(sorted(ping))
        if 'IP blocked' in actions and nicks in nickUser and nickUser[nicks].country:
            actions = actions.replace('IP blocked', 'IP from %s blocked' % nickUser[nicks].country, 1)
        if 'was ' in actions and ', ' in nicks or ' and ' in nicks:
            actions = re.sub(r'\bwas\b', 'were', actions)
        message = '%s %s %s%s' % (nicks, actions, channels, ping and '\x0315 ping %s\x03' % ping or '')
        if re.search('was \x0303(kick|removed|banned)', actions) and nicks in nickUser:
            inchans = [c for c in chanNicks if nicks in chanNicks[c]]
            if channels in inchans:
                message += ' and rejoined'
            elif inchans and not (nicks in nickUser and nickUser[nicks].account in chanList['#wikimedia-ops']['access']):
                message += ', they are also in ' + mklist(len(inchans) > 11 and
                        inchans[0:9] + ['%d more channels' % (len(inchans) - 9)] or inchans)
        bot.msg(chan, message)

def iswarned(target, action, channel, sec=60):
    now = time.time()
    for t, a, c, tm in warned:
        if (t, a, c) == (target, action, channel) and tm + sec > now:
            return True
    return False

def join(user, channel):
    "Called when an user join a channel"
    identhost = '%(ident)s@%(host)s' % user
    ra = recentActions[identhost]
    ra.appendleft(('join', channel, time.time(), None))
    towarn = []
    ping = ''
    identIP = reIdentIP.search(user.full)
    if tools.isinlist(user, 'track'):
        entries = mklist(entry + ('comment' in args and ' "%s"' % args['comment'] or '')
                for entry, args in chanList['global']['track'].iteritems() if (user.account == entry[3:]
                if entry[0:3] == '$a:' else '!' in entry and maskre(entry).match(user.full)))
        towarn.append('tracked (%s)' % entries)
        ping += 't'
    elif user.host.startswith(wikimediaCloaks) or tools.isinlist(user, 'exempt'):
        return
    elif identhost in tracked and tracked[identhost][1] + 3600 > time.time():
        towarn.append('recently ' + tracked[identhost][0])
        ping += 't'
    elif ('ip' in user or identIP) and user.score < 10:
        ip = user.ip or '%d.%d.%d.%d' % tuple(int(identIP.group(1)[i:i + 2], 16) for i in (0, 2, 4, 6))
        blocks = tools.wikiBlock(ip)
        if blocks:
            towarn.append('IP blocked in ' + blocks)
            if any(w for w in blocks.split() if not w.startswith(('(', 'bg'))): # don't ping for bgwikis
                ping += 'w'
            wikis = ' '.join(set(re.findall('global|\w+wiki', blocks)))
            tools.logAction(user=user, channel=channel, action='ip-block', target=wikis)
    target = user.full
    globalBans = [ban for ban, mask in tools.chanBans['#wikimedia-bans'] if mask.match(target)]
    if user.account:
        accban = '$a:' + irclower(user.account)
        globalBans += [ban for ban in chanList['#wikimedia-bans']['ban'] if ban[0] == '$' and irclower(ban) == accban]
    if globalBans:
        towarn.append('global banned (%s)' % ', '.join(globalBans))
        ping += 'g'
        ra.appendleft(('gbanned join', channel, time.time(), None))
    action = '(%s) \x0307joined\x03' % mklist(towarn)
    if towarn and ('tracked' in towarn or not iswarned(user.nick, action, channel, 86400)):
        warn(user.nick, action, channel, ping)

def part(user, channel, reason=''):
    "Called when an user leaves a channel"
    identhost = '%(ident)s@%(host)s' % user
    ra = recentActions[identhost]
    match = re.match(r'requested by (\S+)', reason)
    if match:
        ra.appendleft(('removed', channel, time.time(), user.nick))
        rt = time.time()
        recentKRB.appendleft(('removed', user.full, channel, match.group(1), rt))
        tools.logAction(user=nickUser[match.group(1)], channel=channel, action='remove', target=user)
        for act, banned, chan, by, tm in recentKRB:
            if not (act == 'banned' and tm + 15 > time.time() and (channel == chan or channel in jBans.get(chan, ()))
                    and maskre(banned).match(user.full)) or tm in warnedKRB:
                continue
            warnedKRB.append(rt)
            warnedKRB.append(tm)
            if by == match.group(1):
                warn(user.nick, 'was \x0303banned and removed\x03 by %s from' % match.group(1), channel,
                        warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=1)
            else:
                warn(user.nick, 'was \x0303banned\x03 by %s and \x0303removed\x03 by %s from' % (by, match.group(1)),
                        channel, warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=1)
            kickbanned(user, channel)
            break
        else:
            def removeWarn():
                for act, chan, tm, nick in ra:
                    if act == 'kickban' and tm + 15 > time.time():
                        break
                else:
                    warn(user.nick, 'was \x0303removed\x03 by %s from' % match.group(1), channel,
                            warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=0)
                    warnedKRB.append(rt)
            reactor.callLater(15, removeWarn)
        return
    ra.appendleft(('part', channel, time.time(), user.nick))
    if tools.isinlist(user, 'track'):
        warn(user.nick, '\x0303parted\x03', channel, delay=1)
        return
    timeCheck = time.time() - 5
    last5sec = [act for act, chan, tm, arg in ra if chan == channel and tm > timeCheck]
    if 'join' in last5sec and 'msg' in last5sec:
        warn(user.nick, '\x0307spammed (join-say-part)\x03', channel, 's',
                warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops')
    elif user.full == 'Sigyn!sigyn@freenode/utility-bot/sigyn':
        bot.msg('#wikimedia-ops', 'Sigyn parted from ' + channel)

def quit(user, channels, reason):
    "Called when an user quit"
    identhost = '%(ident)s@%(host)s' % user
    recentActions[identhost].appendleft(('quit', reason, time.time(), user.nick))
    if reason.startswith('Killed ('):
        by = reason.split('(')[1].strip()
        if by.endswith('.freenode.net') or reason == 'Killed (Sigyn (BANG!))':
            return
        warn(user.nick, 'was \x0303killed\x03 by %s in' % by, channels, delay=1)
    elif reason == 'K-Lined':
        warn(user.nick, 'was \x0303K-Lined\x03 in', channels, delay=1)
    elif tools.isinlist(user, 'track'):
        warn(user.nick, '\x0303quit\x03 (%s) from' % reason, channels, delay=1)

def msg(user, channel, msg):
    "Called when an user send a message to a channel"
    # ignore users with affiliated cloak
    if user.get('cloak', 'unaffiliated') != 'unaffiliated' or tools.isinlist(user, 'exempt') \
            or chanNicks[channel].get(user.nick):
        return
    now = time.time()
    ra = recentActions['%(ident)s@%(host)s' % user]
    ra.appendleft(('msg', channel, now, msg))
    if msg.count('\x03') > 5:
        warn(user.nick, '\x0307color spam\x03', channel, 's',
                warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops')
        if chanNicks[channel]['wmopbot'] >= 2:
            autokick(user, channel, 'color spam')
        return
    if len(reASCII.findall(msg)) > 5 and len(ra) > 1 and (ra[1][0], ra[1][1]) == ('msg', channel) and \
            len(reASCII.findall(ra[1][3])) > 5 and len(msg) / (len(re.findall(r'\w', msg)) + 1) > 2:
        warn(user.nick, '\x0307ASCII art\x03 in', channel, 's',
                warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops')
        if chanNicks[channel]['wmopbot'] >= 2:
            autokick(user, channel, 'ASCII art spam')
        return
    if (msg.count('\xc3') + msg.count('\xc2')) * 3 > len(msg) > 20:
        warn(user.nick, '\x0307unicode spam\x03', channel, 's',
                warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops')
        return
    nohttp = re.sub(r'https?://\S+', '', msg)
    repChar = re.search(r'(\S)\1{8,}', nohttp)
    tooConsonants = re.search(r'[bcdfghjklmnpqrstvwxz]{8,}', nohttp)
    blmatch = blacklist and blacklist.search(nohttp)
    m = blmatch or repChar or tooConsonants
    if m:
        action =  blmatch and 'said a \x0307blacklisted term\x03' or repChar and '\x0307repeated character\x03' \
                or tooConsonants and '\x0307many consective consonants\x03'
        start = re.search(r'\S+ ?$', msg[0:m.start()])
        end = re.search(r'^ ?\S+', msg[m.end():])
        match = (start and start.group(0) or '') + '\x1f' + m.group(0)[0:100] + '\x1f' + (end and end.group(0) or '')
        if user.get('score', 0) < 2:
            warn(user.nick, '%s (%s\x0f) in' % (action, match), channel, 'b',
                    warnChan=not channel.startswith(('#wikimedia-opbot', '#wikimedia-overflow'))
                    and channel != '#wikimedia-ops' and '#wikimedia-ops', delay=1)
            tools.logAction(user=user, channel=channel, action='blacklist', target=m.group(0), note=msg)
        else:
            warn('%s (score %d)' % (user.nick, user.get('score', 0)), '%s (%s\x0f) in' % (action, match[0:100]), channel,
                    False, delay=1)
        return
    if len([(a, t) for a, c, t, m in ra if c == channel and a == 'msg' and t + 20 > now]) > 10 and \
            not iswarned(user.nick, '\x0307flooding\x03', channel):
        warn(user.nick, '\x0307flooding\x03', channel, 's',
                warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops')
        return
    if len(ra) > 2 and (ra[1][0], ra[1][1], ra[1][3]) == ('msg', channel, msg) == (ra[2][0], ra[2][1], ra[2][3]):
        warn(user.nick, '\x0307repeating message\x03 "%s" in' % (len(msg) > 50 and msg[0:47] + '...' or msg),
                channel, 's', warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops')
        return
    pings = len(set(msg.split()) & chanNicks[channel].viewkeys())
    if pings > 5 or pings > 2 and any(1 for a, c, t, m in ra if c == channel and a == 'ping3' and t + 20 > now):
        warn(user.nick, '\x0307ping spam\x03', channel, 's',
                warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops')
        if chanNicks[channel]['wmopbot'] >= 2:
            autokick(user, channel, 'ping spam')
        return
    if pings > 2:
        ra.appendleft(('ping3', channel, now, None))

def renamed(user, oldNick):
    "Called when an user nick is changed"
    recentActions['%(ident)s@%(host)s' % user].appendleft(('renamed', None, time.time(), oldNick))

def notice(user, channel, msg):
    "Called when an user send a notice to a channel"
    if user.get('cloak', 'unaffiliated') != 'unaffiliated' or tools.isinlist(user, 'exempt'):
        return
    recentActions['%(ident)s@%(host)s' % user].appendleft(('notice', None, time.time(), msg))
    if user.account in ('wmopbot', 'ChanServ', 'Sigyn', 'eir'):
        return
    if not iswarned(user.nick, '\x0307sent notice\x03 to', channel):
        if channel.startswith('#wikimedia-opbot') or 'AntiSpamMeta' in chanNicks[channel]:
            warn(user.nick, '\x0307sent notice\x03', channel, 's')
        else:
            warn(user.nick, '\x0307sent notice\x03', channel, 's', warnChan='#wikimedia-ops')

def banned(user, channel, target):
    "Called when I see a ban action"
    if target == '*!*@*':
        return
    if '$' in target[1:]:
        target = target[0:target.find('$', 1)]
    bt = time.time()
    recentKRB.appendleft(('banned', target, channel, user.nick, bt))
    if target.startswith('$a:'):
        account = target[3:]
        banUsers = [u for u in nickUser.itervalues() if u.account == account]
    elif target[0] != '$' and '!' in target:
        mask = maskre(target)
        if not mask:
            return
        banUsers = [u for u in nickUser.itervalues() if mask.match(u.full)]
    else:
        return
    banChans = [channel] + jBans.get(channel, [])
    for target in banUsers:
        ra = recentActions['%(ident)s@%(host)s' % target]
        ra.appendleft(('banned', channel, time.time(), target))
        for act, kicked, chan, by, tm in recentKRB:
            if not (act in ('kicked', 'removed') and chan in banChans and target.full.endswith(kicked.split('!')[1])
                    and tm + 15 > time.time()) or tm in warnedKRB:
                continue
            warnedKRB.append(tm)
            warnedKRB.append(bt)
            tgt = target.nick + (target.ip and ' (%s)' % target.ip or '')
            if act == 'kicked' and by == user.nick:
                warn(tgt, 'was \x0303kickbanned\x03 by %s from' % user.nick, chan,
                        warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=1)
            elif act == 'removed' and by == user.nick:
                warn(tgt, 'was \x0303removed and banned\x03 by %s from' % user.nick, chan,
                        warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=1)
            else:
                warn(tgt, 'was \x0303%s\x03 by %s and \x0303banned\x03 by %s from' % (act, by, user.nick),
                        chan, warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=1)
            kickbanned(target, channel)

def kicked(user, channel, target, reason):
    "Called when I see a kick action"
    ra = recentActions['%(ident)s@%(host)s' % target]
    ra.appendleft(('kicked', channel, time.time(), target.nick))
    kt = time.time()
    recentKRB.appendleft(('kicked', target.full, channel, user.nick, kt))
    tgt = target.nick + (target.ip and ' (%s)' % target.ip or '')
    for act, banned, chan, by, tm in recentKRB:
        if not (act == 'banned' and tm + 15 > time.time() and (channel == chan or channel in jBans.get(chan, ()))
                and maskre(banned).match(target.full)) or tm in warnedKRB:
            continue
        if by == user.nick:
            warn(tgt, 'was \x0303kickbanned\x03 by %s from' % user.nick, channel,
                    warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=1)
        else:
            warn(tgt, 'was \x0303banned\x03 by %s and \x0303kicked\x03 by %s from' % (by, user.nick), channel,
                    warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=1)
        warnedKRB.append(kt)
        warnedKRB.append(tm)
        kickbanned(target, channel)
        break
    else:
        def kickWarn():
            for act, chan, tm, nick in ra:
                if act == 'kickban' and tm + 15 > time.time():
                    break
            else:
                warn(tgt, 'was \x0303kicked\x03 by %s from' % user.nick, channel,
                        warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops', delay=0)
                tracked['%(ident)s@%(host)s' % target] = ('kicked from ' + channel, time.time())
                warnedKRB.append(kt)
        reactor.callLater(15, kickWarn)

def kickbanned(target, channel):
    "Called after kickban warning"
    identhost = '%(ident)s@%(host)s' % target
    ra = recentActions[identhost]
    ra.appendleft(('kickban', channel, time.time(), target.nick))
    tracked[identhost] = ('kickbanned from ' + channel, time.time())
    ftarget = target.full
    globalBans = [ban for ban, mask in tools.chanBans['#wikimedia-bans'] if mask.match(ftarget)]
    sugBan = 'ip' in target and ['*!*@' + (target.host.startswith('gateway/') and '*' or '') + target.ip] or []
    if target.account:
        accban = '$a:' + target.account
        globalBans += [ban for ban in chanList['#wikimedia-bans']['ban'] if ban == accban]
        sugBan += [accban]
    sugBan = [b for b in sugBan if not b in globalBans]
    if len({chan for act, chan, tm, arg in ra if act == 'kickban' and not chan.startswith('#wikimedia-opbot')}) > 1 \
            and sugBan and not iswarned(identhost, 'suggest gban', '#wikimedia-ops'):
        message = 'Two recent kickbans for %s, I suggest to global ban %s \x0315ping %s' % (target.nick,
                mklist(sugBan), mklist(sorted(mkping('#wikimedia-bans', False, inchan='#wikimedia-ops'))))
        warned.append((identhost, 'suggest gban', '#wikimedia-ops', time.time()))
        print '[%s] WARN: #wikimedia-ops %s' % (time.strftime('%Y-%m-%d %H:%M:%S'), message)
        reactor.callLater(2, bot.msg, '#wikimedia-ops', message)

def topic(user, channel, new):
    if user.host == 'services.':
        return
    if user.get('cloak', 'unaffiliated') != 'unaffiliated' or tools.isinlist(user, 'exempt') \
            or chanNicks[channel].get(user.nick):
        return
    if not 't' in chanList[channel]['config']['cs/irc'].get('modes', '') and not 'cloak' in user \
            and not tools.isinlist(user, 'exempt'):
        warn(user.nick, '(%s) \x0307changed the topic\x03' % ('unreg' in user and 'unregistered' or 'unaffiliated'),
                channel, 's', warnChan=not channel.startswith('#wikimedia-opbot') and '#wikimedia-ops')

def autokick(target, channel, reason='', ban=False):
    "Process kickban after first checks and whois query processed"
    print 'warns kickban', target.full, channel, reason
    if channel == '#wikimedia-overflow':
        print 'Skiping autokick in -overflow'
        return
    if not channel in chanList or not 'o' in chanList[channel]['access'].get('wmopbot', {}).get('flags', '').lower() \
            and chanNicks[channel]['wmopbot'] < 2:
        print 'Error: autokick where wmopbot is not op'
        return
    if target.score > 100:
        print 'Error: autokick on high scored user', target
        return
    if ban:
        ban = 'account' in target and '$a:' + target.account or 'ip' in target and \
               (target.host.startswith('gateway/') and '*!*@*' or '*!*@') + target['ip'] or '*!*@' + target.host
        if ban == '*!*@*': # for safety
            print '*!*@* ban error'
            ban = False
    if ban:
        bot.sendLine('MODE %s +b %s' % (channel, ban))
    bot.sendLine('KICK %s %s %s' % (channel, target.nick, reason and ':' + reason))
    print 'applying auto kick' + (ban and 'ban' or '')
    if 'eir' in chanNicks[channel]:
        try:
            tools.eirComment[('ban', ban)] = 'wmopbot autokick' + (reason and ' (%s)' % reason)
        except:
            print 'Error on set eir comment'
