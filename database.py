# -*- coding: utf-8 -*-

from twisted.internet import reactor
from collections import deque, defaultdict
from urllib2 import urlopen
import time, json, re, oursql, os

wikimediaCloaks = ('wikipedia/', 'wikimedia/', 'wikibooks/', 'wikinews/', 'wikiquote/', 'wiktionary/',
        'wikisource/', 'wikivoyage/', 'wikiversity/', 'wikidata/', 'mediawiki/')

def irclower(s):
    "IRC lower case: acording RFC 2812, the characters {}|^ are the respective lower case of []\~"
    return s.lower().replace('{', '[').replace('}', ']').replace('|', '\\').replace('^', '~')

def irccompare(a, b):
    "Compare two nicks using IRC rules to lower case, e.g. '[abc]' == '{abc}'"
    return a == b or irclower(a) == irclower(b)

mask2re = {'\\': '[\\\\|]', '|': '[\\\\|]', '^': '[~^]', '~': '[~^]', '[': '[[{]', ']': '[]}]', '{': '[[{]',
        '}': '[]}]', '*': '[^!@]*', '?': '[^!@]', '+': '\\+', '.': '\\.', '(': '\\(', ')': '\\)', '$': '\\$'}

def maskre(mask):
    "Transforms an IRC mask into a regex pattern"
    mask = irclower(mask)
    r = ''
    for c in mask:
        r += mask2re.get(c, c)
    try:
        return re.compile(r + '$', re.I)
    except:
        return None

def mklist(items):
    "Tranforms a sequence ['a', 'b', 'c'] into 'a, b and c' "
    if type(items) != list:
       items = list(items)
    return len(items) > 1 and ', '.join(items[:-1]) + ' and ' + items[-1] or items and items[0] or ''

def addhook(hook, func, *errhooks):
    "Add a hook to hooks list"
    hookList.append((time.time(), hook, func) + errhooks)

def callhook(hook):
    "Calls all correspondent hooks"
    with open('hooks.log', 'a') as f:
        f.write('[%s] %s\n' % (time.strftime('%Y-%m-%d %H:%M:%S'), hook))
    for item in list(hookList):
        t, _hook, func = item[:3]
        if t + 10 < time.time():
            hookList.remove(item)
        elif hook == _hook:
            func()
            hookList.remove(item)
        elif len(item) > 3 and hook in item[3:]:
            func(hook)
            hookList.remove(item)

class dbConn:
    "Connect to bot database"

    def __init__(self):
        self.connection = None
        self.connect()

    def connect(self):
        try:
            self.connection.close()
        except:
            pass
        self.connection = oursql.connect(db='s53213__wmopbot', host='tools.labsdb',
                read_default_file=os.path.expanduser('~/replica.my.cnf'), read_timeout=20, use_unicode=False,
                autoreconnect=True, autoping=True)
        self.cursor = self.connection.cursor()
        self.last = time.time()

    def execute(self, sql, params=()):
        try:
            self.cursor.execute(sql, params)
            self.last = time.time()
        except oursql.OperationalError: # probably connection lost
            if self.last + 10 < time.time():
                self.connect()
                self.cursor.execute(sql, params)

    def query(self, sql, params=()):
        self.execute(sql, params)
        return self.cursor.fetchall()

def _initLists():
    "Populate data lists with SQL database data"
    global userFlags
    sep = re.compile(r'^;|(?<=[^\\]);')
    db = dbConn()
    # list: channel (50), type (10), target (90), args (300), timestatmp (datetime)
    for channel, _type, target, args in db.query('SELECT * FROM list'):
        chanList[channel].setdefault(_type, {})[target] = \
                {k: v.replace('\;', ';') for k, v in (i.split(':', 1) for i in sep.split(args))}
    userFlags = {u: set(chanList['global']['flags'][u]['flags'].split()) for u in chanList['global'].get('flags', ())}
    # lang: key(30), message(255), user(16), timestamp
    for lg_key, msg in db.query('SELECT lg_key, lg_message FROM lang'):
        lang, key = lg_key.split('.', 1)
        msgLang.setdefault(key, {})[lang] = msg
    # $j bans
    for chan in chanList:
        for target in chanList[chan]['ban']:
            if target.startswith('$j:#'):
                jBans[irclower(target[3:])].append(chan)

def dbSync(force=False):
    "Save data in database"
    start = time.time()
    now = time.strftime('%Y-%m-%d %H:%M:%S')
    # list: channel (50), type (10), target (90), args (300)
    current = {(chan, _type, target, ';'.join(':'.join((k, v.replace(';', '\;')))
            for k, v in chanList[chan][_type][target].items()))
            for chan in chanList for _type in chanList[chan] for target in chanList[chan][_type]}
    indb = set(db.query('SELECT * FROM list'))
    delete = indb - current
    insert = list(current - indb)
    del current, indb # free memory
    with open('dbupdates.log', 'a') as f:
        if delete:
            f.write('[%s] DELETE FROM list\n' % time.strftime('%Y-%m-%d %H:%M:%S'))
            f.writelines(repr(line) + '\n' for line in delete)
            db.execute('DELETE FROM list WHERE (ls_channel, ls_type, ls_target) IN (%s)' % ','.join(
                    '(?,?,?)' for d in delete), tuple(i for d in delete for i in d[:3]))
        if insert:
            f.write('[%s] INSERT INTO list\n' % time.strftime('%Y-%m-%d %H:%M:%S'))
            f.writelines(repr(line) + '\n' for line in insert)
            n = 0
            while n < len(insert):
                db.execute('INSERT INTO list VALUES %s' % ','.join('(?,?,?,?)' for n in insert[n:n + 1000]),
                        tuple(i for x in insert[n:n + 1000] for i in x))
                n += 1000
    uploadusers = [u.toDB() for u in nickUser.itervalues() if not u.issaved]
    while uploadusers:
        part = uploadusers[:50]
        del uploadusers[:50]
        db.execute('REPLACE INTO user VALUES ' + ','.join(('(?,?,?,?,NOW(),?,?)',) * len(part)),
                tuple(i for u in part for i in u))

def dumpArgs(d):
  return ';'.join(':'.join((k, v.replace(';', '\;'))) for k, v in d.items())

def regAction(user, channel, action, target, args=None):
    "insert action into db"
    # actions: id(int), user(90), chan(50), action(10), target(90), args(255), timestamp(datetime)
    db.query('INSERT INTO actions (ac_user, ac_chan, ac_action, ac_target, ac_args) (?, ?, ?, ?, ?)',
            (user, channel, action, target, args))

def langMsg(lang, msg, *args):
    if msg in msgLang:
        if isinstance(lang, dict):
            lang = lang.get('lang', 'en')
        elif lang.startswith('#'):
            lang = chanList[lang]['config'].get('LANG', {}).get('prefix', 'en')
        message = lang in msgLang[msg] and msgLang[msg][lang] or msgLang[msg].get('en')
        if not message:
            return msg
        if args and message.count('%s') == len(args):
            return message % args
        return message
    return msg

def getWmChannels():
    global wmChannels
    api = urlopen('https://meta.wikimedia.org/w/api.php?action=query&format=json&prop=revisions&titles=IRC/Channels&rvprop=content')
    data = json.loads(api.read())
    text = data['query']['pages']['6237']['revisions'][0]['*']
    wmChannels = {'#' + str(chan).lower() for chan in re.findall(r'\n\| ?\{\{[Cc]hannel\|([^ |}]+)\}\}', text)}
    for chan in wmChannels:
        if not chan in chanList:
            chanList[chan]['config']['WM'] = {'time': '%d' % time.time(), 'user': 'wmopbot',
                    'comment': 'got automaticaly from https://meta.wikimedia.org/wiki/IRC/Channels'}
    for chan in chanList:
        if chan not in wmChannels:
            del chanList[chan]['config']['WM']

def statsMsg(channel):
    "increment channel message statistics"
    hour = int(time.time()) / 3600
    hourChanStats[hour][channel] += 1

def ststsUser(channel, num):
    "register number of users in channel"
    now = int(time.time())
    last = chanUserStats[channel] and chanUserStats[channel][-1]
    if last and last[0] + 10 > now:
        chanUserStats[channel][-1] = (now, num)
    else:
        chanUserStats[channel].append((now, num))

if not 'chanNicks' in globals():
    db = dbConn()
    chanNicks =  defaultdict(dict)
    nickUser = {}
    chanConfig = defaultdict(dict)
    chanList = defaultdict(lambda: {'access': {}, 'ban': {}, 'quiet': {}, 'mode': {}, 'exempt': {}, 'config': {}})
    msgLang = defaultdict(dict)
    offUsers = []
    blackList = []
    jBans = defaultdict(list)
    commQueue = defaultdict(list)
    opCommands = deque(maxlen=10)
    recentSpam = deque(maxlen=10)
    hourChanStats = defaultdict(lambda: defaultdict(int))
    hookList = []
    topics = {}
    getWmChannels()
    _initLists()
else:
    print '[%s] Reloaded database.py' % time.strftime('%Y-%m-%d %H:%M:%S')
